from django.contrib.auth import login, authenticate
from django.contrib.admin import widgets
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect
from django.forms.forms import ValidationError
from django.utils.translation import ugettext_lazy as _


from assignments.forms import UserForm, LoginForm, ProjectForm
from assignments.models import UserDetails, Project


def index(request, *args, **kwargs):
    return render(request, 'templates/index.html')

def register(request, *args, **kwargs):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = User.objects.create_user(
                username=data['username'], 
                email=data['email'], 
                password=data['password'])
            user_details = UserDetails.objects.create(user=user, user_type=data['user_type'])
            login(request, user)
            return redirect(dashboard)
    else:
        form = UserForm()
    return render(request, 'templates/register.html', context={'form': form})

def register_tutor(request, *args, **kwargs):
    form = UserForm()
    return render(request, 'templates/register_tutor.html', context={'form': form})

def user_login(request, *args, **kwargs):
    if request.user.is_authenticated():
        return redirect(dashboard)

    elif request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(username=data['username'], password=data['password'])
            if user is not None:
                login(request, user)
                details=UserDetails.objects.filter(user=request.user)[0]
                if details.user_type is "student":
                    return redirect(dashboard)
                #else:
                   # return redirect(dashboard_tutor)
    else:
        form = LoginForm()
    return render(request, 'templates/login.html', context={'form': form})

@login_required
def dashboard(request, *args, **kwargs):
    context = {'username': request.user.username.title()}
    
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.instance
            project.user = request.user
            project.save()
            form.clean()
            return redirect(projects)
    else:
        form = ProjectForm()

    for _, v in form.fields.items():
        v.widget.attrs['class'] = 'form-control'

    context['form'] = form
    return render(request, 'templates/dashboard.html', context=context)

@login_required
def projects(request, *args, **kwargs):
    context = {'username': request.user.username.title()}
    projects = Project.objects.filter(user=request.user)
    context["projects"]=projects
    return render(request, 'templates/projects.html', context=context)