from django.db import models
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField


class UserDetails(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	status = models.BooleanField(default=False)
	user_type = models.CharField(max_length=15, 
	                             default='student', 
								 choices=(('Tutor', 'tutor'), ('Student', 'student')))

class Subject(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name.title()

class Budget(models.Model):
	name = models.CharField(max_length=40)

	def __str__(self):
		return self.name.title()

class Project(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	details = RichTextUploadingField()
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
	title = models.CharField(max_length=50)
	deadline = models.DateField()
	budget = models.ForeignKey(Budget, on_delete=models.CASCADE)
	