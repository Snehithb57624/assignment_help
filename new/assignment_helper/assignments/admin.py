from django.contrib import admin

from assignments.models import *

admin.site.register(UserDetails)
admin.site.register(Project)
admin.site.register(Subject)
admin.site.register(Budget)