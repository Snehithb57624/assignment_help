from django import forms
from django.forms.forms import ValidationError


from assignments.models import Project


class UserForm(forms.Form):
    email = forms.EmailField()
    username = forms.CharField()
    password = forms.CharField(max_length=16, widget=forms.PasswordInput)
    confirm_password = forms.CharField(max_length=16, widget=forms.PasswordInput)
    user_type = forms.ChoiceField(choices=(('Student', 'student'), ('Tutor', 'tutor')))

    def clean(self):
        if (self.cleaned_data.get('password') != self.cleaned_data.get('confirm_password')):
            raise ValidationError("Passwords must match!")

        return self.cleaned_data

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(max_length=16, widget=forms.PasswordInput)

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['details', 'subject', 'title', 'deadline', 'budget']