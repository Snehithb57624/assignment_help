from django.contrib.auth.views import logout
from django.conf.urls import url, include

from assignments import views

urlpatterns = [
    url(r'dashboard', views.dashboard),
    # url(r'dashboard_tutor', views.dashboard_tutor), 
    url(r'projects', views.projects), 
    
    url(r'register', views.register),
    url(r'register_tutor', views.register_tutor),
    
    url(r'login', views.user_login),
    url(r'logout', logout, {'next_page': views.index}),
    
    url(r'', views.index),
]